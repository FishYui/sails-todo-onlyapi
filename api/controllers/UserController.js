module.exports = {
    users: async (req, res) => {
        // 找所有Users

        try {
            const response = await User.find({});

            res.status(200).json({ success: true, message: `Success Found Users!`, response: response });
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    user: async (req, res) => {
        // 找單一個User
        const userId = req.params.userId;

        try {
            const response = await User.findOne({ id: userId });

            if (response) {
                res.status(200).json({ success: true, message: `Success Found User!`, response: response });
            } else {
                res.status(400).json({ success: false, message: `Not Found UserId: ${userId}` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    create: async (req, res) => {
        // 新增User
        const username = req.body.username;
        const password = req.body.password;
        const fullname = req.body.fullname;

        if (username == undefined)
            res.status(400).json({ success: false, message: `No Input Username!` });
        else if (password == undefined)
            res.status(400).json({ success: false, message: `No Input Password!` });
        else if (fullname == undefined)
            res.status(400).json({ success: false, message: `No Input Fullname!` });
        else {
            try {
                const response = await User.create({ username: username, password: password, fullname: fullname }).fetch();

                res.status(200).json({ success: true, message: `Success Create User!`, response: response });
            } catch (err) {
                res.status(500).json({ success: false, message: err.message });
            }
        }
    },
    delete: async (req, res) => {
        // 刪除User
        const userId = req.params.userId;

        try {
            const response = await User.destroyOne({ id: userId });

            if (response) {
                res.status(200).json({ success: true, message: `Success Delete UserId: ${userId}` });
            } else {
                res.status(400).json({ success: false, message: `Not Found UserId: ${userId}` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    update: async (req, res) => {
        // 修改User
        const userId = req.params.userId;
        const username = req.body.username;
        const password = req.body.password;
        const fullname = req.body.fullname;
        let updateData = {};

        if (username)
            updateData['username'] = username;
        if (password)
            updateData['password'] = password;
        if (fullname)
            updateData['fullname'] = fullname;

        try {
            const response = await User.updateOne({ id: userId }).set(updateData);

            if (response) {
                res.status(200).json({ success: true, message: `Success Update User`, response: response });
            } else {
                res.status(400).json({ success: false, message: `Not Found UserId: ${userId}` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    }
}
module.exports = {
    login: async (req, res) => {
        // 登入: 先判斷帳號是否存在(資料庫有回傳)，在判斷回傳的密碼與輸入的密碼是否一樣
        const username = req.body.username;
        const password = req.body.password;

        if (username == undefined)
            res.status(400).json({ success: false, message: `No Input Username!` });
        else if (password == undefined)
            res.status(400).json({ success: false, message: `No Input Password!` });
        else {
            try {
                const response = await User.findOne({ username: username });

                if (response) {

                    if (response.password === password) {
                        res.status(200).json({ success: true, message: `Login Success!` });
                    } else {
                        res.status(400).json({ success: false, message: `Password Error!` });
                    }

                } else {
                    res.status(400).json({ success: false, message: `Not Found Username: ${username}` });
                }
            } catch (err) {
                res.status(500).json({ success: false, message: err.message });
            }
        }
    },
    logout: async (req, res) => {
        // 登出: doSomething
        try {
            res.status(200).json({ success: true, message: `Logout Success!` });
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    }
}
module.exports = {
    todos: async (req, res) => {
        // 找所有Todo
        try {
            const response = await Todo.find({});

            res.json({ success: true, message: `Success Found Todos!`, response: response });
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    todo: async (req, res) => {
        // 找單一個Todo
        const todoId = req.params.id;

        try {
            const response = await Todo.findOne({ id: todoId });

            if (response) {
                res.status(200).json({ success: true, message: `Success Found Todo!`, response: response });
            } else {
                res.status(400).json({ success: false, message: `Not Found TodoId: ${todoId}` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    todosByUser: async (req, res) => {
        // 依照使用者找所有Todo
        const userId = req.params.userId;

        try {
            const response = await User.findOne({ id: userId }).populate('todos');

            if (response) {
                res.status(200).json({ success: true, message: `Success Found Todos!`, response: response.todos });
            } else {
                res.status(400).json({ success: false, message: `Not Found UserId: ${userId}` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    create: async (req, res) => {
        // 新增Todo
        const userId = req.body.owner;
        const title = req.body.title;

        if (userId == undefined)
            res.status(400).json({ success: false, message: `No Input UserId!` });
        else if (title == undefined)
            res.status(400).json({ success: false, message: `No Input Todo's Title!` });
        else {
            try {
                const response = await Todo.create({ title: title, owner: userId }).fetch();

                res.status(200).json({ success: true, message: `Success Create Todo!`, response: response });
            } catch (err) {
                res.status(500).json({ success: false, message: err.message });
            }
        }
    },
    delete: async (req, res) => {
        // 依照ID刪除Todo
        const todoId = req.params.id;

        try {
            const response = await Todo.destroyOne({ id: todoId });
            
            if (response) {
                res.status(200).json({ success: true, message: `Success Delete TodoId: ${todoId}` });
            } else {
                res.status(400).json({ success: false, message: `Not Found TodoId: ${todoId}` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    update: async (req, res) => {
        // 依照ID修改Todo
        const todoId = req.params.id;
        let updateData = {};

        if (req.body.title)
            updateData['title'] = req.body.title;
        if (req.body.status)
            updateData['status'] = req.body.status;

        try {
            const response = await Todo.updateOne({ id: todoId }).set(updateData);

            if (response) {
                res.status(200).json({ success: true, message: `Success Update Todo`, response: response });
            } else {
                res.status(400).json({ success: false, message: `Not Found TodoId: ${todoId}` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    }
}
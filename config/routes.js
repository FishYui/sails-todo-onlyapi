/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // Todo Api
  'get /todos': 'TodoController.todos',
  'get /todo/:id': 'TodoController.todo',
  'get /todos/:userId/findByUser': 'TodoController.todosByUser',
  'post /todo': 'TodoController.create',
  'delete /todo/:id': 'TodoController.delete',
  'put /todo/:id': 'TodoController.update',
  // User Api
  'get /users': 'UserController.users',
  'get /user/:userId': 'UserController.user',
  'post /user': 'UserController.create',
  'delete /user/:userId': 'UserController.delete',
  'put /user/:userId': 'UserController.update',
  // Portal Api
  'post /portal/login': 'PortalController.login',
  'get /portal/logout': 'PortalController.logout'

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};

# TodoApp

### Todo Api
  - [**Get All Todos**](#get-all-todos)
  - [**Get All Todos By UserId**](#get-all-todos-by-userid)
  - [**Get One Todo**](#get-one-todo)
  - [**Delete One Todo**](#delete-one-todo)
  - [**Update One Todo**](#update-one-todo)
  - [**Create One Todo**](#create-one-todo)
### User Api
  - [**Get All Users**](#get-all-users)
  - [**Get One User**](#get-one-user)
  - [**Delete One User**](#delete-one-user)
  - [**Update One User**](#update-one-user)
  - [**Create One User**](#create-one-user)
### Portal Api
  - [**User LogIn**](#user-login)
  - [**User LogOut**](#user-logout)
### Models
  - [**Todo**](#todo)
  - [**User**](#user)

---

**Get All Todos**
----
  Returns All Todos.

* **URL**
  
  `/todos`

* **Method:**
  
  `GET`

* **Parameters:**  
  
  `None`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Found Todos!, response: *Todos[] }`
 
* **Error Response:**

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Get All Todos By UserId**
----
  Returns All of Specified User's Todos.

* **URL**
  
  `/todos/{userId}/findByUser`

* **Method:**
  
  `GET`

* **Parameters:** 
  * **Path:**
  
    `userId = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Found Todos!, response: *Todos[] }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found UserId: *userId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Get One Todo**
----
  Returns Specified Todo.

* **URL**
  
  `/todo/{todoId}`

* **Method:**
  
  `GET`

* **Parameters:** 
  * **Path:**
  
    `todoId = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Found Todo!, response: *Todo }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found TodoId: *todoId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Delete One Todo**
----
  Delete Specified Todo.

* **URL**
  
  `/todo/{todoId}`

* **Method:**
  
  `DELETE`

* **Parameters:** 
  * **Path:**
  
    `todoId = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Delete TodoId: *todoId }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found TodoId: *todoId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Update One Todo**
----
  Update Specified Todo.

* **URL**
  
  `/todo/{todoId}`

* **Method:**
  
  `PUT`

* **Parameters:** 
  * **Path:**
  
    `todoId = string, required`

  * **FormData**
  
    `title = string`

    `status = boolean`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Update TodoId: *todoId, response: *Todo }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found TodoId: *todoId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Create One Todo**
----
  Create A New Todo.

* **URL**
  
  `/todo`

* **Method:**
  
  `POST`

* **Parameters:** 
  * **FormData**
  
    `ownerId = string, required`

    `title = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Create Todo!, response: *Todo }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input OwnerId! }`

  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input Todo's Title! }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Get All Users**
----
  Returns All Users.

* **URL**
  
  `/users`

* **Method:**
  
  `GET`

* **Parameters:**  
  
  `None`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Found Users!, response: *Users[] }`
 
* **Error Response:**

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Get One User**
----
  Returns Specified User.

* **URL**
  
  `/user/{userId}`

* **Method:**
  
  `GET`

* **Parameters:** 
  * **Path:**
  
    `userId = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Found User!, response: *User[] }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found UserId: *userId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Delete One User**
----
  Delete Specified User.

* **URL**
  
  `/user/{userId}`

* **Method:**
  
  `DELETE`

* **Parameters:** 
  * **Path:**
  
    `userId = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Delete UserId: *userId }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found UserId: *userId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Update One User**
----
  Update Specified User.

* **URL**
  
  `/user/{userId}`

* **Method:**
  
  `PUT`

* **Parameters:** 
  * **Path:**
  
    `userId = string, required`

  * **FormData**
  
    `password = string`

    `fullname = string`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Update UserId: *userId, response: *User }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found UserId: userId }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Create One User**
----
  Create A New User.

* **URL**
  
  `/user`

* **Method:**
  
  `POST`

* **Parameters:** 
  * **FormData**

    `username = string, required`

    `password = string, required`

    `fullname = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Success Create User!, response: *User }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input Username! }`

  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input Password! }`

  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input Fullname! }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**User LogIn**
----
  Login.

* **URL**
  
  `/portal/login`

* **Method:**
  
  `POST`

* **Parameters:** 
  * **FormData**

    `username = string, required`

    `password = string, required`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Login Success! }`
 
* **Error Response:**
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input Username! }`

  * **Code:** 400 <br />
    **Content:** `{ success: false, message: No Input Password! }`
  
  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Not Found Username: *username }`

  * **Code:** 400 <br />
    **Content:** `{ success: false, message: Password Error! }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**User LogOut**
----
  Logout.

* **URL**
  
  `/portal/logout`

* **Method:**
  
  `GET`

* **Parameters:** 
  
  `None`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ success: true, message: Logout Success! }`

  * **Code:** 500 <br />
    **Content:** `{ success: false, message: *Error_message }`

---

**Todo**
----
  * _id: ObjectId
  * createdAt: timestamp
  * updatedAt: timestamp
  * title: string
  * status: boolean
  * owner: ObjectId(User)

---

**User**
----
  * _id: ObjectId
  * createdAt: timestamp
  * updatedAt: timestamp
  * username: string
  * password: string
  * fullname: string